package pl.codementors.unicorns;

import org.apache.commons.lang3.tuple.Pair;

/**
 * Application main class.
 *
 * @author psysiu
 */
public class UnicornsMain {

    /**
     * Application starting method.
     *
     * @param args Application starting arguments.
     */
    public static void main(String[] args) {
        System.out.println("Hello in unicorn world!");
    }

}
