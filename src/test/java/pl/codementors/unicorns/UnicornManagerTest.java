package pl.codementors.unicorns;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.*;
import java.util.Collection;
import java.util.Collections;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
//import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.*;
/**
 * @author psysiu
 */
@RunWith(PowerMockRunner.class)
public class UnicornManagerTest {

    UnicornManager manager;
    Unicorn unicorn1, unicorn2, unicorn3, unicorn4;

    @Rule
    public ExpectedException exceptionGrabber = ExpectedException.none();

    @Before
    public void prepare() {
        manager = new UnicornManager();
        unicorn1 = new Unicorn("RainbowDash");
        unicorn2 = new Unicorn("ShiningArmor");
        unicorn3 = new Unicorn("RainbowDash");
        unicorn4 = new Unicorn("ShiningArmor");
    }

    @Test
    public void Unicorn_created_emptyCollection() {
        assertNotNull("collection should exist", manager.getUnicorns());
        assertThat("Collection should be empty", manager.getUnicorns().isEmpty(), is(true));
    }

    @Test
    public void add_paramProvided_collectionWithOneElement() {
        manager.add(unicorn1);
        assertThat(manager.getUnicorns().size(), is(1));
        assertThat(manager.getUnicorns(), hasItem(unicorn1));
    }

    @Test
    public void pair_twoDifferentUnicorns_pairedSuccessfully() throws UnicornAlreadyPairedException {
        manager.add(unicorn1);
        manager.add(unicorn2);
        manager.pair(unicorn1, unicorn2);

        assertEquals(unicorn2, manager.getPaired(unicorn1));
        assertEquals(unicorn1, manager.getPaired(unicorn2));
    }

    @Test
    public void getPaired_differentReferences_returnPairedUnicorns() throws UnicornAlreadyPairedException {
        manager.add(unicorn1);
        manager.add(unicorn2);
        manager.pair(unicorn1, unicorn2);

        assertEquals(unicorn2, manager.getPaired(unicorn3));
        assertEquals(unicorn1, manager.getPaired(unicorn4));
    }

    @Test
    public void pair_pairingNotAddedUnicorns_noPairingDone() throws UnicornAlreadyPairedException {
        manager.pair(unicorn1, unicorn2);

        assertNull(manager.getPaired(unicorn1));
        assertNull(manager.getPaired(unicorn2));
    }

    @Test
    public void pair_pairAlreadyPairedUnicorns_throwException() throws UnicornAlreadyPairedException {
        manager.add(unicorn1);
        manager.add(unicorn2);
        manager.pair(unicorn1, unicorn2);
        exceptionGrabber.expect(UnicornAlreadyPairedException.class);
        exceptionGrabber.expectMessage("At least one of the unicorns already paired.");
        manager.pair(unicorn1, unicorn2);
    }

    @Test
    public void load_FileDoesNotExist_throwException() throws UnicornException {
        File input = mock(File.class);
        when(input.exists()).thenReturn(false);

        exceptionGrabber.expect(UnicornException.class);
        exceptionGrabber.expectCause(instanceOf(FileNotFoundException.class));
        manager.load(input);
    }

    @Test
    public void load_FileIsDirectory_throwExepction() throws UnicornException {

        File input = mock(File.class);
        when(input.exists()).thenReturn(true);
        when(input.isDirectory()).thenReturn(true);

        exceptionGrabber.expect(UnicornException.class);
        exceptionGrabber.expectCause(instanceOf(IOException.class));
        manager.load(input);
    }

    @Test
    public void load_FileIsNotReadable_throwExepction() throws UnicornException {

        File input = mock(File.class);
        when(input.exists()).thenReturn(true);
        when(input.isDirectory()).thenReturn(false);
        when(input.canRead()).thenReturn(false);

        exceptionGrabber.expect(UnicornException.class);
        exceptionGrabber.expectCause(instanceOf(IOException.class));
        manager.load(input);
    }

    @Test
    @PrepareForTest(UnicornManager.class)
    public void load_OneUnicornInFileNoSpace_OneUnicornRead() throws UnicornException, IOException, Exception {
        File input = mock(File.class);
        when(input.exists()).thenReturn(true);
        when(input.isDirectory()).thenReturn(false);
        when(input.canRead()).thenReturn(true);

        FileReader fr = mock(FileReader.class);
        BufferedReader br = mock(BufferedReader.class);
        when(br.readLine()).thenReturn("RainbowDash").thenReturn(null);

        whenNew(FileReader.class).withArguments(input).thenReturn(fr);
        whenNew(BufferedReader.class).withArguments(fr).thenReturn(br);

        manager.load(input);

        Unicorn unicorn = new Unicorn("RainbowDash");
        assertThat(manager.getUnicorns(), hasItem(unicorn));
    }

    @Test
    @PrepareForTest(UnicornManager.class)
    public void load_OneUnicornInFileSpace_OneUnicornRead() throws UnicornException, IOException, Exception {
        File input = mock(File.class);
        when(input.exists()).thenReturn(true);
        when(input.isDirectory()).thenReturn(false);
        when(input.canRead()).thenReturn(true);

        FileReader fr = mock(FileReader.class);
        BufferedReader br = mock(BufferedReader.class);
        when(br.readLine()).thenReturn("Rainbow Dash").thenReturn(null);

        whenNew(FileReader.class).withArguments(input).thenReturn(fr);
        whenNew(BufferedReader.class).withArguments(fr).thenReturn(br);

        manager.load(input);

        Unicorn unicorn = new Unicorn("Rainbow Dash");
        assertThat(manager.getUnicorns(), hasItem(unicorn));
    }

    @Test
    @PrepareForTest(UnicornManager.class)
    public void load_TwoUnicornsWithNoSpaceInName_TwoUnicornRead() throws UnicornException, IOException, Exception {
        File input = mock(File.class);
        when(input.exists()).thenReturn(true);
        when(input.isDirectory()).thenReturn(false);
        when(input.canRead()).thenReturn(true);

        FileReader fr = mock(FileReader.class);
        BufferedReader br = mock(BufferedReader.class);
        when(br.readLine()).thenReturn("RainbowShine").thenReturn("RainbowBug").thenReturn(null);

        whenNew(FileReader.class).withArguments(input).thenReturn(fr);
        whenNew(BufferedReader.class).withArguments(fr).thenReturn(br);

        manager.load(input);

        Unicorn unicorn1 = new Unicorn("RainbowShine");
        Unicorn unicorn2 = new Unicorn("RainbowBug");
        assertThat(manager.getUnicorns(), hasItems(unicorn1, unicorn2));
    }


    @Test
    @PrepareForTest(UnicornManager.class)
    public void load_TwoUnicornsWithSpaceInName_TwoUnicornRead() throws UnicornException, IOException, Exception {
        File input = mock(File.class);
        when(input.exists()).thenReturn(true);
        when(input.isDirectory()).thenReturn(false);
        when(input.canRead()).thenReturn(true);

        FileReader fr = mock(FileReader.class);
        BufferedReader br = mock(BufferedReader.class);
        when(br.readLine()).thenReturn("Rainbow Shine").thenReturn("Rainbow Bug").thenReturn(null);

        whenNew(FileReader.class).withArguments(input).thenReturn(fr);
        whenNew(BufferedReader.class).withArguments(fr).thenReturn(br);

        manager.load(input);

        Unicorn unicorn1 = new Unicorn("Rainbow Shine");
        Unicorn unicorn2 = new Unicorn("Rainbow Bug");
        assertThat(manager.getUnicorns(), hasItems(unicorn1, unicorn2));
    }

    @Test
    public void remove_TryRemoveUnicornThatNotExistInManager_UnicornInMenagerStayTheSame() throws UnicornException {

        Unicorn unicorn1 = new Unicorn("Rainbow Shine");
        Unicorn unicorn2 = new Unicorn("Rainbow Bug");
        Unicorn unicorn3 = new Unicorn("RainbowShine");
        manager.add(unicorn1);
        manager.add(unicorn2);
        int sizeBeforeDelete = manager.getUnicorns().size();
        manager.remove(unicorn3);
        assertThat(manager.getUnicorns(), hasItems(unicorn1, unicorn2));
        assertEquals(sizeBeforeDelete, manager.getUnicorns().size());
    }

    @Test
    public void remove_DeleteAddedUnicorn_UnicornDeletedFromUnicornManager() throws UnicornException {

        Unicorn unicorn1 = new Unicorn("Rainbow Shine");
        Unicorn unicorn2 = new Unicorn("Rainbow Bug");
        manager.add(unicorn1);
        manager.add(unicorn2);
        int sizeBeforeDelete = manager.getUnicorns().size();
        manager.remove(unicorn2);
        assertThat(manager.getUnicorns(), not(hasItem(unicorn2)));
        assertEquals(sizeBeforeDelete - 1, manager.getUnicorns().size());
    }

    @Test
    public void remove_DeleteNullFromUnicornManager_UnicornInMenagerStayTheSame() throws UnicornException {

        Unicorn unicorn1 = new Unicorn("Rainbow Shine");
        Unicorn unicorn2 = new Unicorn("Rainbow Bug");
        manager.add(unicorn1);
        manager.add(unicorn2);
        int sizeBeforeDelete = manager.getUnicorns().size();
        manager.remove(null);
        assertThat(manager.getUnicorns(), hasItems(unicorn1, unicorn2));
        assertEquals(sizeBeforeDelete, manager.getUnicorns().size());
    }

    @Test
    public void add_AddNullToUnicornManager_UnicornInMenagerStayTheSameNoException() {

        Unicorn unicorn1 = new Unicorn("Rainbow Shine");
        Unicorn unicorn2 = null;
        manager.add(unicorn1);
        int sizeBeforeAddNull = manager.getUnicorns().size();


        manager.add(unicorn2);


        assertThat(manager.getUnicorns(), hasItem(unicorn1));
        assertThat(manager.getUnicorns(), not(hasItem(unicorn2)));
        assertEquals(sizeBeforeAddNull, manager.getUnicorns().size());
    }

    @Test
    public void add_AddExistUnicornToUnicornManager_UnicornInMenagerStayTheSame() {

        Unicorn unicorn1 = new Unicorn("Rainbow Shine");
        Unicorn unicorn2 = new Unicorn("Rainbow Shine");
        manager.add(unicorn1);
        int sizeBeforeAddSameElement = 1;


        manager.add(unicorn2);

        System.out.println(manager.getUnicorns().size());

        assertThat(manager.getUnicorns(), hasItem(unicorn1));
        assertEquals(sizeBeforeAddSameElement, manager.getUnicorns().size());
    }

    @Test
    public void remove_RemovePairedUnicornInUnicornManager_UnicornPairedException() throws UnicornException {

        Unicorn unicorn1 = new Unicorn("Rainbow Shine");
        Unicorn unicorn2 = new Unicorn("Rainbow NoShine");
        manager.add(unicorn1);
        manager.add(unicorn2);

        manager.pair(unicorn1, unicorn2);

        exceptionGrabber.expect(UnicornException.class);
        exceptionGrabber.expectCause(instanceOf(UnicornAlreadyPairedException.class));
        manager.remove(unicorn1);
    }

    @Test
    public void unpair_UnpairUnicornsInSameOrderInPaired_UnicornUnpaired() throws UnicornException {

        Unicorn unicorn1 = new Unicorn("Rainbow Shine");
        Unicorn unicorn2 = new Unicorn("Rainbow NoShine");
        manager.add(unicorn1);
        manager.add(unicorn2);
        manager.pair(unicorn1, unicorn2);

        manager.unpair(unicorn1, unicorn2);

        assertThat(manager.getPaired(unicorn1), equalTo(null));
        assertThat(manager.getPaired(unicorn2), equalTo(null));
    }


    @Test
    public void unpair_UnpairUnicornsDifferentOrderInPaired_UnicornUnpaired() throws UnicornException {

        Unicorn unicorn1 = new Unicorn("Rainbow Shine");
        Unicorn unicorn2 = new Unicorn("Rainbow NoShine");
        manager.add(unicorn1);
        manager.add(unicorn2);

        manager.pair(unicorn1, unicorn2);
        manager.unpair(unicorn2, unicorn1);

        assertThat(manager.getPaired(unicorn1), equalTo(null));
        assertThat(manager.getPaired(unicorn2), equalTo(null));
    }


    @Test
    public void unpair_UnpairUnicornsWithDifferentReferencesSameOrder_UnicornUnpaired() throws UnicornException {

        Unicorn unicorn1 = new Unicorn("Rainbow Shine");
        Unicorn unicorn2 = new Unicorn("Rainbow NoShine");
        Unicorn unicorn3 = new Unicorn("Rainbow Shine");
        Unicorn unicorn4 = new Unicorn("Rainbow NoShine");
        Unicorn unicorn5 = new Unicorn("Rainbow Shine");
        Unicorn unicorn6 = new Unicorn("Rainbow NoShine");
        manager.add(unicorn1);
        manager.add(unicorn2);

        manager.pair(unicorn3, unicorn4);
        manager.unpair(unicorn5, unicorn6);

        assertThat(manager.getPaired(unicorn1), equalTo(null));
        assertThat(manager.getPaired(unicorn2), equalTo(null));
    }

    @Test
    public void unpair_UnpairUnicornsWithDifferentReferencesDifferentOrder_UnicornUnpaired() throws UnicornException {

        Unicorn unicorn1 = new Unicorn("Rainbow Shine");
        Unicorn unicorn2 = new Unicorn("Rainbow NoShine");
        Unicorn unicorn3 = new Unicorn("Rainbow Shine");
        Unicorn unicorn4 = new Unicorn("Rainbow NoShine");
        Unicorn unicorn5 = new Unicorn("Rainbow Shine");
        Unicorn unicorn6 = new Unicorn("Rainbow NoShine");
        manager.add(unicorn1);
        manager.add(unicorn2);

        manager.pair(unicorn3, unicorn4);
        manager.unpair(unicorn6, unicorn5);

        assertThat(manager.getPaired(unicorn1), equalTo(null));
        assertThat(manager.getPaired(unicorn2), equalTo(null));
    }

    @Test
    public void unpair_UnpairUnicornWithAlreadyPairedUnicorn_UnicornManagerUnchanged() throws UnicornException {

        Unicorn unicorn1 = new Unicorn("Rainbow Shine");
        Unicorn unicorn2 = new Unicorn("Rainbow NoShine");
        Unicorn unicorn3 = new Unicorn("Fake one");
        manager.add(unicorn1);
        manager.add(unicorn2);
        manager.add(unicorn3);

        manager.pair(unicorn1, unicorn2);
        manager.unpair(unicorn1, unicorn3);

        assertThat(manager.getPaired(unicorn1), equalTo(unicorn2));
        assertThat(manager.getPaired(unicorn2), equalTo(unicorn1));
    }


    @Test
    public void unpair_UnpairUnicornWithAlreadyPairedSecondUnicorn_UnicornManagerUnchanged() throws UnicornException {

        Unicorn unicorn1 = new Unicorn("Rainbow Shine");
        Unicorn unicorn2 = new Unicorn("Rainbow NoShine");
        Unicorn unicorn3 = new Unicorn("Fake one");
        manager.add(unicorn1);
        manager.add(unicorn2);
        manager.add(unicorn3);

        manager.pair(unicorn1, unicorn2);
        manager.unpair(unicorn3, unicorn2);

        assertThat(manager.getPaired(unicorn1), equalTo(unicorn2));
        assertThat(manager.getPaired(unicorn2), equalTo(unicorn1));
    }

    @Test
    public void getUnicorns_ReturnUnicorns_UnicornsUnchangeable() throws UnicornException {

        Unicorn unicorn1 = new Unicorn("Rainbow Shine");
        Unicorn unicorn2 = new Unicorn("Rainbow NoShine");
        Unicorn unicorn3 = new Unicorn("Right one");
        Unicorn unicorn4 = new Unicorn("FAKE ONE");
        manager.add(unicorn2);
        manager.add(unicorn3);
        manager.add(unicorn1);

        for (Unicorn u : manager.getUnicorns()) {
            u = unicorn4;
        }

        assertThat(manager.getUnicorns(), not(hasItems(unicorn4)));
    }


    @Test
    public void getUnicornPairs_AfterInstantiationUnicornManager_EmptyCollection() throws UnicornException {

        Collection<Pair<Unicorn, Unicorn>> pairedUnicorns = manager.getUnicornPairs();

        assertThat(pairedUnicorns.size(), is(0));
        assert (pairedUnicorns.isEmpty());
    }

    @Test
    public void getUnicornPairs_ReturnCollectionUnchangeable_UnchangeableCollection() throws UnicornException {

        Collection<Pair<Unicorn, Unicorn>> pairedUnicorns = manager.getUnicornPairs();

        Unicorn unicorn1 = new Unicorn("AA");
        Unicorn unicorn2 = new Unicorn("BB");

        exceptionGrabber.expect(UnsupportedOperationException.class);

        pairedUnicorns.add(new ImmutablePair<>(unicorn1, unicorn2));
        assertThat(pairedUnicorns.size(), is(0));
        assert (pairedUnicorns.isEmpty());
    }

    @Test
    public void getUnicornPairs_PairUnicornsAndGetThem_PairedUnicornsCollection() throws UnicornException {

        Unicorn unicorn1 = new Unicorn("Rainbow Shine");
        Unicorn unicorn2 = new Unicorn("Rainbow NoShine");
        Unicorn unicorn3 = new Unicorn("Good one");
        Unicorn unicorn4 = new Unicorn("EvenBetter one");
        manager.add(unicorn1);
        manager.add(unicorn2);
        manager.add(unicorn3);
        manager.add(unicorn4);

        manager.pair(unicorn1, unicorn2);
        manager.pair(unicorn3, unicorn4);


        Collection<Pair<Unicorn, Unicorn>> pairedUnicorns = manager.getUnicornPairs();

        assertThat(pairedUnicorns.size(), is(2));
        assertThat(manager.getPaired(unicorn1), equalTo(unicorn2));
        assertThat(manager.getPaired(unicorn2), equalTo(unicorn1));
        assertThat(manager.getPaired(unicorn3), equalTo(unicorn4));
        assertThat(manager.getPaired(unicorn4), equalTo(unicorn3));
    }
}